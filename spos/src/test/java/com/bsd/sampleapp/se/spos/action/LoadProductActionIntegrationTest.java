package com.bsd.sampleapp.se.spos.action;


import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bsd.sampleapp.se.spos.device.LcdService;
import com.bsd.sampleapp.se.spos.model.Product;
import com.bsd.sampleapp.se.spos.service.CartService;
import com.bsd.sampleapp.se.spos.service.CartServiceImpl;
import com.bsd.sampleapp.se.spos.service.DataFormatter;
import com.bsd.sampleapp.se.spos.service.LcdDataFormatter;
import com.bsd.sampleapp.se.spos.service.ProductRepositoryServiceImpl;
import com.bsd.sampleapp.se.spos.service.ScannerService;
import com.bsd.sampleapp.se.spos.service.ScannerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class LoadProductActionIntegrationTest {
	
	@Mock
	private ProductRepositoryServiceImpl productRepositoryService;
	
	@Mock
	private LcdService lcdService;

	@InjectMocks
	private LoadProductAction loadProductAction;
	
	private ScannerService scannerService = new ScannerServiceImpl();
	
	private DataFormatter lcdDataFormatter = new LcdDataFormatter();
	
	private CartService cartService = new CartServiceImpl();
	
	@Test
	public void shouldLoadTwoProductsToCart() {
		loadProductAction.setScannerService(scannerService);
		loadProductAction.setDataFormatter(lcdDataFormatter);
		loadProductAction.setCartService(cartService);
		Optional<Product> product1 = Optional.of( new Product("aaa111", "water 1.5L", 3.00) );
		Optional<Product> product2 = Optional.of( new Product("bbb222", "beer paulaner 0.5L", 7.00) );
		
		when( productRepositoryService.findByCode("aaa111") ).thenReturn( product1 );
		loadProductAction.run( product1.get().getCode().getBytes(StandardCharsets.UTF_8) );
		
		when( productRepositoryService.findByCode("bbb222") ).thenReturn( product2 );
		loadProductAction.run( product2.get().getCode().getBytes(StandardCharsets.UTF_8) );
		
		verify( lcdService, times(1) ).sendOut( contains(product1.get().getName()) );
		verify( lcdService, times(1) ).sendOut( contains(product2.get().getName()) );
	}
	
}
