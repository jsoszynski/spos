package com.bsd.sampleapp.se.spos.action;

import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bsd.sampleapp.se.spos.device.PrinterService;
import com.bsd.sampleapp.se.spos.model.Product;
import com.bsd.sampleapp.se.spos.service.CartService;
import com.bsd.sampleapp.se.spos.service.CartServiceImpl;
import com.bsd.sampleapp.se.spos.service.DataFormatter;
import com.bsd.sampleapp.se.spos.service.PrinterDataFormatter;

@ExtendWith(MockitoExtension.class)
class CompleteSaleActionIntegrationTest {
	
	@Mock
	private PrinterService printerService;

	@InjectMocks
	private CompleteSaleAction completeSaleAction;
	
	private DataFormatter receiptFormatter = new PrinterDataFormatter();
	
	private CartService cartService = new CartServiceImpl();
	
	private static List<Product> products = new ArrayList<>();

	@BeforeAll
	static void setUp() throws Exception {
		products.add( new Product("aaa111", "water staropolanka 1.5L", 3.00) );
		products.add( new Product("bbb222", "beer paulaner 0.5L", 7.00) );
		products.add( new Product("ccc333", "juice tomato pudliszki", 5.00) );
	}

	@Test
	void shouldReceiptBePrinted() {
		completeSaleAction.setDataFormatter(receiptFormatter);
		completeSaleAction.setCartService(cartService);
		cartService.clean();
		cartService.add( products.get(0) );
		cartService.add( products.get(1) );
		cartService.add( products.get(2) );
		byte[] dataFromExit = null;
		
		completeSaleAction.run(dataFromExit);
		verify( printerService ).sendOut( contains(products.get(0).getName()) );
		verify( printerService ).sendOut( contains(products.get(1).getName()) );
		verify( printerService ).sendOut( contains(products.get(2).getName()) );
		verify( printerService ).sendOut( contains("razem: 15.0") );
	}
	
	@Test
	void shouldReceiptDuplicateBePrinted() {
		completeSaleAction.setDataFormatter(receiptFormatter);
		completeSaleAction.setCartService(cartService);
		cartService.clean();
		cartService.add( products.get(0) );
		cartService.add( products.get(1) );
		cartService.add( products.get(2) );
		byte[] dataFromExit = null;
		
		completeSaleAction.run(dataFromExit);
		verify( printerService ).sendOut( contains(products.get(0).getName()) );
		verify( printerService ).sendOut( contains(products.get(1).getName()) );
		verify( printerService ).sendOut( contains(products.get(2).getName()) );
		verify( printerService ).sendOut( contains("razem: 15.0") );
		
		//second run, when next LoadProductRunAction has not been run yet (after previous sale complete)
		reset( printerService );
		completeSaleAction.run(dataFromExit);
		verify( printerService ).sendOut( contains(products.get(0).getName()) );
		verify( printerService ).sendOut( contains(products.get(1).getName()) );
		verify( printerService ).sendOut( contains(products.get(2).getName()) );
		verify( printerService ).sendOut( contains("razem: 15.0") );
	}

}
