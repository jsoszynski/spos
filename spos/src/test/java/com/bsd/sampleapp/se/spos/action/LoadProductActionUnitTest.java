package com.bsd.sampleapp.se.spos.action;


import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.bsd.sampleapp.se.spos.device.LcdService;
import com.bsd.sampleapp.se.spos.service.ProductRepositoryServiceImpl;
import com.bsd.sampleapp.se.spos.service.ScannerService;
import com.bsd.sampleapp.se.spos.service.ScannerServiceImpl;

@ExtendWith(MockitoExtension.class)
public class LoadProductActionUnitTest {
	
	@Mock
	private ProductRepositoryServiceImpl productRepositoryService;
	
	@Mock
	private LcdService lcdService;

	@InjectMocks
	private LoadProductAction loadProductAction;
	
	private ScannerService scannerService = new ScannerServiceImpl();
	
	@Test
	public void shouldBeBarCodeInvalid() {
		loadProductAction.setScannerService(scannerService);
		
		String emptyCode = " ";
		loadProductAction.run( emptyCode.getBytes(StandardCharsets.UTF_8) );
		verify( lcdService, times(1) ).sendOut( contains("Invalid bar-code") );
	}
	
	@Test
	public void shouldProductNotFound() {
		loadProductAction.setScannerService(scannerService);
		
		String barCode = "xxl";
		when( productRepositoryService.findByCode(barCode) ).thenReturn( Optional.empty() );
		loadProductAction.run( barCode.getBytes(StandardCharsets.UTF_8) );
		verify( lcdService, times(1) ).sendOut( contains("Product not found") );
	}
	
}
