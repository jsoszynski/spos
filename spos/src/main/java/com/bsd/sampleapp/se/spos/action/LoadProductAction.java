package com.bsd.sampleapp.se.spos.action;

import java.util.Arrays;

import com.bsd.sampleapp.se.spos.model.Product;
import com.bsd.sampleapp.se.spos.model.Status;
import com.bsd.sampleapp.se.spos.repository.SaleStatus;

/**
 * Facade to load product to cart action.
 * 
 *@author JS 2018-10-30
 *
 */
public class LoadProductAction extends Action {
	
	@Override
	public void run(byte[] scannedData) {
		String receivedCode = scannerService.getCode( scannedData );
		String messageToDisplay = makeActionAndGetDisplayMassage(receivedCode);
		//temp
		System.out.println("LCD:     " + messageToDisplay);
		outputDeviceService.sendOut( messageToDisplay );
	}

	private String makeActionAndGetDisplayMassage(String receivedBarCode) {
		if ( receivedBarCode == null || receivedBarCode.trim().isEmpty() ) {
			return "error : Invalid bar-code";
		}
		
		return productRepositoryService.findByCode(receivedBarCode)
				.map( prod -> {			updateCartAndStatus(prod);
										return dataFormatter.getData(Arrays.asList(prod));
							  } )
				.orElse( "error : Product not found" );
	}

	private void updateCartAndStatus(Product scannedProduct) {
		if  ( Status.COMPLETED.equals(SaleStatus.getInstance().getStatus()) ) {
			cartService.clean();
		}
		cartService.add(scannedProduct);
		SaleStatus.getInstance().setStatus(Status.STARTED);
	}

}
