package com.bsd.sampleapp.se.spos.listener;

/**
 * Listeners which should be run following devices arrived std in signal.
 * And should run relevant action. 
 * 
 * @author JS 2018-10-30
 *
 */
public interface InputDeviceListener {
	
	public void dataInputHandler();

}
