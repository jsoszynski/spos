package com.bsd.sampleapp.se.spos.service;

import java.util.Optional;

import com.bsd.sampleapp.se.spos.model.Product;

public class ProductRepositoryServiceImpl implements ProductRepositoryService {

	@Override
	public void add(Product newProduct) {
		//to imlement
	}

	@Override
	public void remove(String productCode) {
		//to implement
	}

	@Override
	public Optional<Product> findByCode(String productCode) {
		//mocked
		return Optional.empty();
	}

	@Override
	public boolean isContains(String productCode) {
		//to implement
		return false;
	}

}
