package com.bsd.sampleapp.se.spos.service;

import java.util.List;

import com.bsd.sampleapp.se.spos.model.Product;
import com.bsd.sampleapp.se.spos.repository.Cart;

/**
 * To manage products in the cardt.
 * 
 * @author JS 2018-10-30
 *
 */
public interface CartService {
	
	Cart cart = Cart.getInstance(); 
	
	/**
	 * Add one product to a cart;
	 * 
	 * @param scannedProduct product which should be added
	 */
	public void add(Product scannedProduct);
	
	/** 
	 * Remove product from a cart.
	 * 
	 * @param productCode code of product which should be removed
	 */
	public void remove(String productCode);
	
	/**
	 * Remove all products from a cart to make cart empty.
	 */
	public void clean();
	
	/**
	 * Get all products stored in the cart.
	 * 
	 * @return list of products
	 */
	public List<Product> getAll();
	
}
