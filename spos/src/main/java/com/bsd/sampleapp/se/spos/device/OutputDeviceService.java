package com.bsd.sampleapp.se.spos.device;

/**
 * To maintain output devices.
 * 
 * @author JS 2018-10-30
 *
 */
public interface OutputDeviceService {
	
	public abstract void sendOut(String data);

}
