package com.bsd.sampleapp.se.spos.action;

import com.bsd.sampleapp.se.spos.device.OutputDeviceService;
import com.bsd.sampleapp.se.spos.service.CartService;
import com.bsd.sampleapp.se.spos.service.DataFormatter;
import com.bsd.sampleapp.se.spos.service.ProductRepositoryService;
import com.bsd.sampleapp.se.spos.service.ScannerService;

/**
 * Facade to run Sale actions.
 * 
 * @author JS 2018-10-30
 *
 */
public abstract class Action {
	
	ScannerService scannerService;

	ProductRepositoryService productRepositoryService;
	
	OutputDeviceService outputDeviceService;
	
	DataFormatter dataFormatter;
	
	CartService cartService;
	
	public abstract void run(byte[] readDataFromInputDevice);

	public void setScannerService(ScannerService scannerService) {
		this.scannerService = scannerService;
	}

	public void setProductRepositoryService(ProductRepositoryService productRepositoryService) {
		this.productRepositoryService = productRepositoryService;
	}
	
	public void setOutputDeviceService(OutputDeviceService outputDeviceService) {
		this.outputDeviceService = outputDeviceService;
	}

	public void setDataFormatter(DataFormatter dataFormatter) {
		this.dataFormatter = dataFormatter;
	}

	public void setCartService(CartService cartService) {
		this.cartService = cartService;
	}
	
}