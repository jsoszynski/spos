package com.bsd.sampleapp.se.spos.service;

import java.nio.charset.StandardCharsets;

public class ScannerServiceImpl implements ScannerService {

	@Override
	public String getCode(byte[] receivedData) {
		
		return new String(receivedData, StandardCharsets.UTF_8);
	}

}
