package com.bsd.sampleapp.se.spos.service;

import java.util.Optional;

import com.bsd.sampleapp.se.spos.model.Product;

/**
 * To manage DB products repository.
 * 
 *@author JS 2018-10-30 
 *
 */
public interface ProductRepositoryService {
	
	public void add(Product newProduct);
	
	public void remove(String productCode);
	
	public Optional<Product> findByCode(String productCode);
	
	public boolean isContains(String productCode);
	
}
