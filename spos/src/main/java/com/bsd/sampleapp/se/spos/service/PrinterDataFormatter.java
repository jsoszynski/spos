package com.bsd.sampleapp.se.spos.service;

import java.util.List;
import java.util.Optional;

import com.bsd.sampleapp.se.spos.model.Product;

public class PrinterDataFormatter implements DataFormatter {

	@Override
	public String getData(List<Product> products) {
		StringBuilder text = new StringBuilder();
		products.forEach( product -> text.append( product.getName() )
											.append( " cena: ")
											.append( product.getPrice() )
											.append( EOL )
						);
		
		Optional<Double> total = products.stream().map(Product::getPrice).reduce(Double::sum);
		text.append( "--------------" + EOL )
			.append( "Koszt razem: ").append( total.orElse(0.00) ).append( EOL );
		
		return text.toString();
	}

}
