package com.bsd.sampleapp.se.spos.service;

import java.util.List;

import com.bsd.sampleapp.se.spos.model.Product;

public class LcdDataFormatter implements DataFormatter {

	@Override
	public String getData(List<Product> products) {
		StringBuilder text = new StringBuilder();
		text.append( products.get(0).getName() )
			.append( " cena: ")
			.append( products.get(0).getPrice() );
		
		return text.toString();
	}

}
