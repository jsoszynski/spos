package com.bsd.sampleapp.se.spos.listener;

import java.nio.charset.StandardCharsets;

import com.bsd.sampleapp.se.spos.action.Action;
import com.bsd.sampleapp.se.spos.action.CompleteSaleAction;
import com.bsd.sampleapp.se.spos.device.PrinterService;
import com.bsd.sampleapp.se.spos.service.CartServiceImpl;
import com.bsd.sampleapp.se.spos.service.PrinterDataFormatter;
import com.bsd.sampleapp.se.spos.service.ProductRepositoryServiceImpl;

/**
 * Listener which should be run when exit signal will arrive from any device (except bar code scanner).
 * 
 * @author JS 2018-10-30
 *
 */
public class ExitListener implements InputDeviceListener {

	@Override
	public void dataInputHandler() {
		Action completeSale = new CompleteSaleAction();
		completeSale.setProductRepositoryService(new ProductRepositoryServiceImpl());
		completeSale.setDataFormatter(new PrinterDataFormatter());
		completeSale.setCartService(new CartServiceImpl());
		completeSale.setOutputDeviceService(new PrinterService());
		
		completeSale.run("anyBarCode".getBytes(StandardCharsets.UTF_8));
	}

}
