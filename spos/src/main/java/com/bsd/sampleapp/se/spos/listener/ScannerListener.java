package com.bsd.sampleapp.se.spos.listener;

import java.nio.charset.StandardCharsets;

import com.bsd.sampleapp.se.spos.action.Action;
import com.bsd.sampleapp.se.spos.action.LoadProductAction;
import com.bsd.sampleapp.se.spos.device.LcdService;
import com.bsd.sampleapp.se.spos.service.CartServiceImpl;
import com.bsd.sampleapp.se.spos.service.LcdDataFormatter;
import com.bsd.sampleapp.se.spos.service.ProductRepositoryServiceImpl;
import com.bsd.sampleapp.se.spos.service.ScannerServiceImpl;

/**
 * Listener which should be run when data arrive will arrive from bar code scanner.
 * 
 * @author JS 2018-10-30
 *
 */
public class ScannerListener implements InputDeviceListener {

	@Override
	public void dataInputHandler() {
		Action loadProductAction = new LoadProductAction();
		loadProductAction.setProductRepositoryService(new ProductRepositoryServiceImpl());
		loadProductAction.setScannerService(new ScannerServiceImpl());
		loadProductAction.setDataFormatter(new LcdDataFormatter());
		loadProductAction.setCartService(new CartServiceImpl());
		loadProductAction.setOutputDeviceService(new LcdService());
		
		loadProductAction.run("anyBarCode".getBytes(StandardCharsets.UTF_8));
	}

}
