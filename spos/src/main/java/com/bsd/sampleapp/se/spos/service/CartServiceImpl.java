package com.bsd.sampleapp.se.spos.service;

import java.util.List;

import com.bsd.sampleapp.se.spos.model.Product;

public class CartServiceImpl implements CartService {

	@Override
	public void add(Product scannedProduct) {
		cart.getProducts().add(scannedProduct);
	}

	@Override
	public void remove(String productCode) {
		cart.getProducts().removeIf( product -> product.getCode().equals(productCode) );
	}

	@Override
	public void clean() {
		cart.getProducts().clear();
	}

	@Override
	public List<Product> getAll() {
		return cart.getProducts();
	}

}
