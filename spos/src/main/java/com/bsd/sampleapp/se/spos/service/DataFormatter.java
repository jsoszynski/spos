package com.bsd.sampleapp.se.spos.service;

import java.util.List;

import com.bsd.sampleapp.se.spos.model.Product;

/**
 * To format data for relevant output device.
 * 
 * @author JS 2018-10-30 
 *
 */
public interface DataFormatter {
	
	static final String EOL = System.getProperty("line.separator");
	
	public String getData(List<Product> products);

}
