package com.bsd.sampleapp.se.spos.repository;

import java.util.ArrayList;
import java.util.List;

import com.bsd.sampleapp.se.spos.model.Product;

/**
 * To store all scanned products to then end of shopping.
 * 
 * @author JS 2018-10-26
 */
public class Cart {
	
	private static Cart instance;
	
	private List<Product> scannedProducts;
	
	private Cart() {
	}
	
	public static synchronized Cart getInstance() {
		if(instance==null) {
			instance = new Cart();
			instance.scannedProducts = new ArrayList<>();
		}
		
		return instance;
	}
	
	public synchronized List<Product> getProducts() {
		return scannedProducts;
	}
	
}

