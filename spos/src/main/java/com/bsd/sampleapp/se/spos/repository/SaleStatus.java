package com.bsd.sampleapp.se.spos.repository;

import com.bsd.sampleapp.se.spos.model.Status;

/**
 * Singleton to store current status of sales process.
 * 
 * @author 2018-10-29
 *
 */
public class SaleStatus {
	
	private static SaleStatus instance;
	
	private Status status;
	
	private SaleStatus() {
	}
	
	public static synchronized SaleStatus getInstance() {
		if ( instance == null ) {
			instance = new SaleStatus();
			instance.status = Status.NOT_STARTED;
		}
		
		return instance;
	}
	
	public synchronized Status getStatus() {
		
		return status;
	} 
	
	public synchronized void  setStatus( Status status ) {
		this.status = status;
	}

}
