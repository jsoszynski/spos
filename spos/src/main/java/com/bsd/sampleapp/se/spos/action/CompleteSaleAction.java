package com.bsd.sampleapp.se.spos.action;

import com.bsd.sampleapp.se.spos.model.Status;
import com.bsd.sampleapp.se.spos.repository.SaleStatus;

/**
 * Facade to run finish sale action.
 * 
 * @author JS 2018-10-30
 *
 */
public class CompleteSaleAction extends Action {

	@Override
	public void run(byte[] scannedData) {
				SaleStatus.getInstance().setStatus(Status.COMPLETED);
				String messageToDisplay = dataFormatter.getData( cartService.getAll() );
				//temp
				System.out.println(">>>\nPRINTER:\n" + messageToDisplay + "<<<");
				outputDeviceService.sendOut( messageToDisplay );
	}

}
