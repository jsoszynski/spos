package com.bsd.sampleapp.se.spos.model;

public enum Status {
	
	NOT_STARTED, 
	STARTED, 
	COMPLETED;
	
}
