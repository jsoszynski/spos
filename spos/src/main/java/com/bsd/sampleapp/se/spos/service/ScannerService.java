package com.bsd.sampleapp.se.spos.service;

/**
 * To make some operations with data received from bar code scanner.
 * 
 * @author JS 2018-10-30 
 *
 */
public interface ScannerService {
	
	public String getCode(byte[] receivedData);
	
}
